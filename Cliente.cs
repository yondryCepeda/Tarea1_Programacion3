﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Cliente
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int saldo { get; set; }
        public string cedula { get; set; }

        public Cliente() { }

        public Cliente(int pid, string pnombre, string papellido, int psaldo, string pcedula)
        {
            this.id = pid;
            this.nombre = pnombre;
            this.apellido = papellido;
            this.saldo = psaldo;
            this.cedula = pcedula;

        }
    }
}
