﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Usuarios u1 = new Usuarios();
     
        public Form1()
        {
            InitializeComponent();
        }

        private void button_ingresar(object sender, EventArgs e)
        {
            
            if (u1.IniciarSeccion(textBox1, textBox2) == true)
            {
                Form2 miFormulario = new Form2();
                this.Hide();
                miFormulario.Show();
            }
            else {
                MessageBox.Show("datos incorrectos intente de nuevo!");
                textBox1.Text = "";
                textBox2.Text = "";
                textBox1.Focus();

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox_nombre(object sender, EventArgs e)
        {

        }

        private void textBox_contraseña(object sender, EventArgs e)
        {

        }

        private void button_salir(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile("icono.png");
        }
    }
}
