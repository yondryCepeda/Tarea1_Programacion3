﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form4 : Form
    {
        
        Form2 f2 = new Form2();
        public int efectivo;
        public int saldoResultante;
        public Form4()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string efectivoRetirado = tmonto.Text;
            efectivo = Int32.Parse(efectivoRetirado);
        }

        private void buttonAtras_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            f2.Show();
        }

        private void ButtonAceptar_Click(object sender, EventArgs e)
        {
            Usuarios u = new Usuarios();
            
            

            if (u.saldo > efectivo){
                MessageBox.Show("Retiro realizado con exito!");
                saldoResultante = u.saldo - efectivo;
                this.Hide();
                f2.Show();
            }
            else {
                MessageBox.Show("Saldo insuficiente, revise su cuenta por favor");
                this.Hide();
                Form2 f22 = new Form2();
                f2.Show();

            }
           
        }
    }
}
